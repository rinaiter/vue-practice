import Vue from 'vue';
import Router from 'vue-router';
import Home from '../components/Home';
import Orders from '../components/User/Orders';
import Login from '../components/Auth/Login';
import Registration from '../components/Auth/Registration';
import AdList from '../components/Ads/AdList';
import Ad from '../components/Ads/Ad';
import NewAd from '../components/Ads/NewAd';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '',
      name: 'home',
      component: Home,
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/registration',
      name: 'reg',
      component: Registration,
    },
    {
      path: '/list',
      name: 'list',
      component: AdList,
    },
    {
      path: '/ad/:id',
      name: 'ad',
      component: Ad,
    },
    {
      path: '/new',
      name: 'newAd',
      component: NewAd,
    },
  ],
  mode: 'history',
});
